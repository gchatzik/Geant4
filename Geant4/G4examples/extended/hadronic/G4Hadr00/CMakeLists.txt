gaudi_depends_on_subdirs(Geant4/G4config)

gaudi_install_scripts()

gaudi_alias(hadronic_tests run_hadronic_tests.py)

# CMT-compatibility alias
gaudi_alias(hard00.exe Hadr00)
