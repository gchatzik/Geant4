#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################
# Autor : Luca Pescatore
# Mail : luca.pescatore@cern.ch
# Purpose : Run iteratively the had00.exe program to get
#           cross sections as a function of energy in Geant4
#           different particles and target pmaterials.
################################################################


import sys
import os
from subprocess import Popen, PIPE
import re
from array import array
from ROOT import *
from tempfile import NamedTemporaryFile

phys_lists = ['FTFP', 'FTFP_BERT', 'QGSP_BERT']
particles = ['pi+', 'pi-', 'kaon+', 'kaon-', 'proton', 'anti_proton']
targets = ['Al', 'Be', 'Si']

template = '''#================================================
#     Macro file for hadr00
#     06.06.2008 V.Ivanchneko
#================================================
/control/verbose 1
/run/verbose 1
/tracking/verbose 0
#
/testhadr/TargetMat        G4_Al
/testhadr/TargetRadius     2  cm
/testhadr/TargetLength     50 cm
/testhadr/PrintModulo      10
#
/testhadr/particle   {particle}
/testhadr/targetElm  {target}
/testhadr/verbose    1
/testhadr/nBinsE      900
/testhadr/nBinsP      700
/testhadr/minEnergy   1 keV
/testhadr/maxEnergy   1 TeV
/testhadr/minMomentum 1 MeV
/testhadr/maxMomentum 10 TeV
#
#/testhadr/HistoType  root
#/testhadr/HistoName  test
/testhadr/verbose 1
#/testhadr/HistoPrint  1
/run/initialize
#
#/run/setCut           1 km
#/gun/particle proton
#/gun/energy 20. GeV
#
/run/beamOn 0
#
'''

def fillGraph(nbins, energy, value) :

    gr = TGraph(int(nbins))
    for i in range(0,len(energy)) :
        gr.SetPoint(i,float(energy[i]),float(value[i]))
    return gr



def process(particle, target, phys_list):
    print "Producing: ", phys_list, particle, target

    with NamedTemporaryFile() as tmp:
        tmp.write(template.format(particle=particle, target=target))
        tmp.flush()
        
        if not os.path.exists("tables"):
            os.makedirs("tables")
        tables = open(os.path.join('tables', '{0}_{1}_{2}.txt'.format(phys_list, particle, target)),"w")
        tables.write("Energy Elastic Inelastic Total\n")

        job = Popen(['hadr00.exe', tmp.name, phys_list], stdout=PIPE)
        
        regex = re.compile(r'^\s*\d+\s+([0-9.]+)\s+([0-9.]+)\s+([0-9.]+)\s+([0-9.]+)')
        found = False
        energy = array('d')
        elastic = array('d')
        inelastic = array('d')
        total = array('d')
        for line in job.stdout:
            if found:
                m = regex.match(line)
                if m:
                    en, el, inel, tot = map(float, m.groups())
                    energy.append(en)
                    elastic.append(el)
                    inelastic.append(inel)
                    total.append(tot)
                    tables.write("{} {} {} {}\n".format(en, el, inel, tot))
            else:
                found = '### Fill Cross Sections' in line

        tables.close()
        nbins = len(energy)

        if not os.path.exists("root"):
            os.makedirs("root")
        output = TFile(os.path.join('root',
                                    '{0}_{1}_{2}.root'.format(phys_list, particle, target)),
                       "RECREATE")

        data = {'Elastic': elastic, 'Inelastic': inelastic, 'Total': total}
        for key, value in data.iteritems():
            gr = fillGraph(nbins, energy, value)
            gr.SetName(key)
            gr.SetTitle("%s cross section for %s interacting with %s" % (key, particle, target))
            gr.GetXaxis().SetTitle("Energy (MeV)")
            gr.GetYaxis().SetTitle("%s cross section (b)" % key)
            gr.SetLineWidth(2)
            gr.Write()
        output.Close()


############# Main: producing graphs

for phys_list in phys_lists :
    for particle in particles :
        for target in targets :
            process(particle, target, phys_list)


############# Plotting graphs on pdf

G4version = os.environ["G4VERS"]

c1 = TCanvas()

part  = ['pi-', 'kaon-', 'proton']
apart = ['pi+', 'kaon+', 'anti_proton']

for p,ap in zip(part,apart) :
    _file = TFile("root/FTFP_BERT_"+p+"_Al.root");
    gr0 = _file.Get("Total");
    _file1 = TFile("root/FTFP_BERT_"+ap+"_Al.root");
    gr1 = _file1.Get("Total");
    _file2 = TFile("root/QGSP_BERT_"+p+"_Al.root");
    gr2 = _file2.Get("Total");
    _file3 = TFile("root/QGSP_BERT_"+ap+"_Al.root");
    gr3 = _file3.Get("Total");

    gr0.SetTitle("Comparison of physics lists in Geant4 " + G4version);
    gr0.GetXaxis().SetTitle("Energy (MeV)");
    gr0.GetYaxis().SetTitle("Total cross section (b)");
    gr0.SetLineWidth(2)
    gr0.SetLineColor(1)
    gr0.SetLineStyle(1)
    if "kaon" in p :
        gr0.SetMaximum(1.5)
    gr0.Draw("AL")
  
    gr1.SetLineWidth(2)
    gr1.SetLineColor(1)
    gr1.SetLineStyle(2)
    gr1.Draw("samesL")
  
    gr2.SetLineWidth(2);
    gr2.SetLineStyle(1)
    gr2.SetLineColor(4);
    gr2.Draw("samesL");

    gr3.SetLineWidth(2);
    gr3.SetLineColor(4);
    gr3.SetLineStyle(2);
    gr3.Draw("samesL");

    legend = TLegend(0.13,0.55,0.36,0.85);
    legend.AddEntry(gr0,"FTFP_BERT  "+p, "l");
    legend.AddEntry(gr1,"FTFP_BERT  "+ap, "l");
    legend.AddEntry(gr2,"QGSP_BERT  "+p, "l");
    legend.AddEntry(gr3,"QGSP_BERT  "+ap, "l");
    legend.SetFillStyle(0);
    legend.Draw();

    c1.SetLogx();
    c1.SetGridx();
    c1.Print("phys_list_comparison_"+p.replace("-","")+".pdf")
    c1.Print("phys_list_comparison_"+p.replace("-","")+".C")


