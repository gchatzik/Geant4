
source /afs/cern.ch/lhcb/software/releases/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh 

SetupProject Geant4 HEAD --nightly lhcb-gauss-dev --user-area /afs/cern.ch/work/t/tiwillia/private/9.6.p04cmtuser 

export TESTAREAROOT="/afs/cern.ch/work/t/tiwillia/private/9.6.p04cmtuser/Geant4_HEAD/GEANT4/GEANT4_HEAD/Geant4/G4examples/extended/electromagnetic/G4TestEm3"

export PLIST="Test/opt1noapplycuts"

$TESTAREAROOT/x86_64-slc6-gcc48-opt/testEm3.exe $TESTAREAROOT/$PLIST/opt1noapplycuts.mac   #generated the data if run with sim option

cp ./NOUTPUT_2.root $TESTAREAROOT/$PLIST/NOUTPUT_2.root

cp ./NOUTPUT_3.root $TESTAREAROOT/$PLIST/NOUTPUT_3.root

cp ./NOUTPUT_4.root $TESTAREAROOT/$PLIST/NOUTPUT_4.root

cp ./NOUTPUT_5.root $TESTAREAROOT/$PLIST/NOUTPUT_5.root

cp ./NOUTPUT_6.root $TESTAREAROOT/$PLIST/NOUTPUT_6.root

cp ./NOUTPUT_7.root $TESTAREAROOT/$PLIST/NOUTPUT_7.root

cp ./NOUTPUT_8.root $TESTAREAROOT/$PLIST/NOUTPUT_8.root

cp ./NOUTPUT_9.root $TESTAREAROOT/$PLIST/NOUTPUT_9.root

cp ./NOUTPUT_10.root $TESTAREAROOT/$PLIST/NOUTPUT_10.root

cp ./NOUTPUT_11.root $TESTAREAROOT/$PLIST/NOUTPUT_11.root

cp ./NOUTPUT_12.root $TESTAREAROOT/$PLIST/NOUTPUT_12.root

cp ./NOUTPUT_13.root $TESTAREAROOT/$PLIST/NOUTPUT_13.root

cp ./NOUTPUT_14.root $TESTAREAROOT/$PLIST/NOUTPUT_14.root

cp ./NOUTPUT_15.root $TESTAREAROOT/$PLIST/NOUTPUT_15.root


g++ -c `root-config --cflags` $TESTAREAROOT/$PLIST/Plot.C
g++ -o $TESTAREAROOT/$PLIST/Plot `root-config --glibs` $TESTAREAROOT/$PLIST/Plot.o

$TESTAREAROOT/$PLIST/Plot

cp ./Save.root $TESTAREAROOT/$PLIST/Save.root

cp Selectedresults.root $TESTAREAROOT/$PLIST/Selectedresults.root

cp ./selectedresults.txt $TESTAREAROOT/$PLIST/selectedresults.txt

#creates full file of plots Save.root and Key results in Selectedresults.root

