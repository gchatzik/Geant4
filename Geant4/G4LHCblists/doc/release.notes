!-----------------------------------------------------------------------------
! Package     : Geant4/G4LHCblists
! Responsible : Gloria CORTI
! Purpose     : Private LHCb physics lists
!----------------------------------------------------------------------------

! 2015-11-02 - Marco Clemencic
 - Added .gitignore file

!======================== G4LHCblists v3r2 2015-04-30 ========================
! 2015-04-30 - Timothy Williams
        - Replaced G4EmStandardPhysics_option1NoApplyCuts.cc and .hh with v9.6.p04
of G4EmStandardPhysics_option1.cc and .hh but with the EmProcessOption of "SetApplyCuts"
removed.  Previous version was v9.4 of G4EmStandardPhysics_option1.cc with "SetApplyCuts" removed.

!======================== G4LHCblists v3r1 2015-03-07 ========================
! 2015-03-07 - Timothy Williams
	- Modified G4EmStandardPhysics_option1NoApplyCuts.cc to use UrbanMsc93 and
	WentzelVI models for multiple scattering as suggested by V.Ivanchenko.

!======================== G4LHCblists v3r0 2013-09-16 ========================
! 2013-09-16 - Gloria Corti
 - Remove obsolete header of physics list that was left over
   . HadronPhysicsQGSP_BERT_HP.hh

!======================== G4LHCblists v2r2 2011-10-03 ========================
! 2011-10-03 - Gloria Corti
 - Add new list with suggestion from Matt Reid for improvement of Multiple
   scattering. It has the same options as the LHCb list.
   . G4EmStandardPhysics_LHCbTest.{hh,cc}

!======================== G4LHCblists v2r1 2011-09-19 ========================
! 2011-09-18 - Gloria Corti
 - Add options in contructor to apply or not the cuts, as to have the
   possibility to have a physics list with old EM behaviour and new MS and to
   apply simple MS to e+/- as provided by A.Dotti to address CMS calorimeter
   issues with this physics list

!======================== G4LHCblists v2r0 2011-07-13 ========================
! 2011-07-13 - Gloria Corti
 - Fix renaming in G4EmStandardPhysics_option1NoApplyCuts.cc

! 2011-07-12 - Gloria Corti
 - Remove old physics list, that are obsolete.
 - Introduce new physics lists for LHCb Em physics set up by V.Ivanchenko
   (G4EmStandardPhysics_option1LHCb.{hh,cc}) with Wentzel model for all
   particles types.
 - Introduce modified version of option1 that does not apply cuts to reproduce
   behaviour as in G4 9.2. (G4EmStandardPhysics_option1NoApplyCuts.{hh,cc})

!============================= G4LHCblists ===================================

! 2005-11-09 - Gloria CORTI
 - New package to hold private LHCb physics lists. It has the QGSP_BERT_HP
   list provided by H-P Wellish for low energy neutron studies

!=============================================================================
